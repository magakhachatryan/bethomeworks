﻿namespace RouletteUI
{
    partial class Roulette
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GoButton = new System.Windows.Forms.Button();
            this.BalanceTextBox = new System.Windows.Forms.TextBox();
            this.ResultTextBox = new System.Windows.Forms.TextBox();
            this.BetComboBox = new System.Windows.Forms.ComboBox();
            this.BalanceLabel = new System.Windows.Forms.Label();
            this.BetLabel = new System.Windows.Forms.Label();
            this.ResultLabel = new System.Windows.Forms.Label();
            this.MesseagetextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // GoButton
            // 
            this.GoButton.Location = new System.Drawing.Point(218, 155);
            this.GoButton.Name = "GoButton";
            this.GoButton.Size = new System.Drawing.Size(132, 31);
            this.GoButton.TabIndex = 0;
            this.GoButton.Text = "GO";
            this.GoButton.UseVisualStyleBackColor = true;
            this.GoButton.Click += new System.EventHandler(this.GoButton_Click);
            // 
            // BalanceTextBox
            // 
            this.BalanceTextBox.Location = new System.Drawing.Point(12, 48);
            this.BalanceTextBox.Name = "BalanceTextBox";
            this.BalanceTextBox.Size = new System.Drawing.Size(121, 22);
            this.BalanceTextBox.TabIndex = 1;
            // 
            // ResultTextBox
            // 
            this.ResultTextBox.Location = new System.Drawing.Point(12, 155);
            this.ResultTextBox.Name = "ResultTextBox";
            this.ResultTextBox.Size = new System.Drawing.Size(100, 22);
            this.ResultTextBox.TabIndex = 2;
            // 
            // BetComboBox
            // 
            this.BetComboBox.FormattingEnabled = true;
            this.BetComboBox.Items.AddRange(new object[] {
            "5",
            "10",
            "15"});
            this.BetComboBox.Location = new System.Drawing.Point(218, 46);
            this.BetComboBox.Name = "BetComboBox";
            this.BetComboBox.Size = new System.Drawing.Size(121, 24);
            this.BetComboBox.TabIndex = 3;
            // 
            // BalanceLabel
            // 
            this.BalanceLabel.AutoSize = true;
            this.BalanceLabel.Location = new System.Drawing.Point(12, 24);
            this.BalanceLabel.Name = "BalanceLabel";
            this.BalanceLabel.Size = new System.Drawing.Size(59, 17);
            this.BalanceLabel.TabIndex = 4;
            this.BalanceLabel.Text = "Balance";
            // 
            // BetLabel
            // 
            this.BetLabel.AutoSize = true;
            this.BetLabel.Location = new System.Drawing.Point(215, 24);
            this.BetLabel.Name = "BetLabel";
            this.BetLabel.Size = new System.Drawing.Size(29, 17);
            this.BetLabel.TabIndex = 5;
            this.BetLabel.Text = "Bet";
            // 
            // ResultLabel
            // 
            this.ResultLabel.AutoSize = true;
            this.ResultLabel.Location = new System.Drawing.Point(12, 128);
            this.ResultLabel.Name = "ResultLabel";
            this.ResultLabel.Size = new System.Drawing.Size(48, 17);
            this.ResultLabel.TabIndex = 6;
            this.ResultLabel.Text = "Result";
            // 
            // MesseagetextBox
            // 
            this.MesseagetextBox.Location = new System.Drawing.Point(12, 238);
            this.MesseagetextBox.Name = "MesseagetextBox";
            this.MesseagetextBox.ReadOnly = true;
            this.MesseagetextBox.Size = new System.Drawing.Size(453, 22);
            this.MesseagetextBox.TabIndex = 7;
            // 
            // Roulette
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(477, 304);
            this.Controls.Add(this.MesseagetextBox);
            this.Controls.Add(this.ResultLabel);
            this.Controls.Add(this.BetLabel);
            this.Controls.Add(this.BalanceLabel);
            this.Controls.Add(this.BetComboBox);
            this.Controls.Add(this.ResultTextBox);
            this.Controls.Add(this.BalanceTextBox);
            this.Controls.Add(this.GoButton);
            this.Name = "Roulette";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Try Your Luck";
            this.Load += new System.EventHandler(this.Roulette_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button GoButton;
        private System.Windows.Forms.TextBox BalanceTextBox;
        private System.Windows.Forms.TextBox ResultTextBox;
        private System.Windows.Forms.ComboBox BetComboBox;
        private System.Windows.Forms.Label BalanceLabel;
        private System.Windows.Forms.Label BetLabel;
        private System.Windows.Forms.Label ResultLabel;
        private System.Windows.Forms.TextBox MesseagetextBox;
    }
}

