﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SeriousPlay;


namespace RouletteUI
{
    public partial class Roulette : Form
    {
        public Roulette()
        {
            InitializeComponent();
        }

        private void Roulette_Load(object sender, EventArgs e)
        {
            this.MesseagetextBox.Hide();
            this.BetComboBox.Text = "0";
            this.BalanceTextBox.Text = RouletteLogic.balance.ToString();
        }

        private void GoButton_Click(object sender, EventArgs e)
        {
            if(RouletteLogic.RotQuantity==5)
            {
                MessageBox.Show("Yourhave reached your limit","",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            else
            {
                this.MesseagetextBox.Hide();
                if (Convert.ToInt32(this.BetComboBox.Text)==0)
                {
                    MessageBox.Show("Please enter your Bet", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    List<int> res = RouletteLogic.Rotate(Convert.ToInt32(this.BetComboBox.Text));
                    this.ResultTextBox.Text = string.Concat(res[0], res[1], res[2]);
                    this.BalanceTextBox.Text = RouletteLogic.balance.ToString();
                    if (res[0] == res[1] && res[1] == res[2])
                    {
                        this.MesseagetextBox.Text = "Congrats.You won  " + this.BetComboBox.Text + " dollars";
                        this.MesseagetextBox.Show();
                    }
                    else
                    {
                        this.MesseagetextBox.Text = "This time you didnt win.Maybe You will be lucky next time";
                        this.MesseagetextBox.Show();

                    }
                }
            }
        }
    }
}
