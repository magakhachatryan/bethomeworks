﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Converter;

namespace ChangeMoney
{
    class Program
    {

        static void Main(string[] args)
        {
            Currency c1 = new Currency("AMD");
            Currency c2 = new Currency("RUB");
            Currency c3 = new Currency("USD");
            Currency c4 = new Currency("GBR");
            Currency c5 = new Currency("EUR");

            try
            {
                Converters.AddNewCurrency(c1);
                Converters.AddNewCurrency(c2);
                Converters.AddNewCurrency(c3);
                Converters.AddNewCurrency(c4);
                Converters.AddNewCurrency(c5);
            }
            catch (CurrencyExistsException ex)
            {
                Console.WriteLine(ex.Message + ": " + ex.Member.Name);
            }

            try
            {
                Converters.AddCourse(c2, c1, 7.4f);
                Converters.AddCourse(c1, c3, 485f);
                Converters.AddCourse(c2, c3, 83f);
                Converters.AddCourse(c2, c5, 85f);
                Converters.AddCourse(c5, c1, 501f);
            }
            catch (CurrencyDoesNotExistsException ex)
            {
                Console.WriteLine(ex.Message + ": " + ex.Member.Name);
            }

            try
            {
                float result = Converters.Calculate(c2, c1, 5000);
                Console.WriteLine(result);
            }
            catch (CurrencyDoesNotExistsException ex)
            {
                Console.WriteLine(ex.Message + ": " + ex.Member.Name);
            }

            Console.ReadKey();
        }
    }
}
