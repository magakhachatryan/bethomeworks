﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Converter
{
    public class CurrencyExistsException : SystemException
    {
        public CurrencyExistsException(string msg, Currency c)
            : base(msg)
        {
            member = c;
        }
        private Currency member;
        public Currency Member { get { return member; } }
    }

    public class CurrencyDoesNotExistsException : SystemException
    {
        public CurrencyDoesNotExistsException(string msg, Currency c)
            : base(msg)
        {
            member = c;
        }
        private Currency member;
        public Currency Member { get { return member; } }
    }
}
