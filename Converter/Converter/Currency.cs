﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Converter
{
    public class Currency
    {
        public readonly Guid ID;
        public string Name { get; private set; }

        public Currency(string name)
        {
            ID = new Guid();
            Name = name;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Currency))
            { return false; }
            Currency c = obj as Currency;
            return c.ID == ID && c.Name == Name;
        }

        static public bool operator ==(Currency c1, Currency c2)
        {
            return c1.Equals(c2);
        }

        static public bool operator !=(Currency c1, Currency c2)
        {
            return !c1.Equals(c2);
        }
    }
}
