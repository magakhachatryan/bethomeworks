﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Converter
{
    using TypeOfValue = List<KeyValuePair<Currency, float>>;

    static public class Converters
    {
        private static Dictionary<Currency, TypeOfValue> CurrencyMapping =
    new Dictionary<Currency, TypeOfValue>();

        static public void AddNewCurrency(Currency c)
        {
            if (CurrencyMapping.ContainsKey(c))
            {
                throw new CurrencyExistsException("Currency already exist", c);
            }
            CurrencyMapping.Add(c, new TypeOfValue());
        }

        static public void AddCourse(Currency c1, Currency c2, float v)
        {
            if (!CurrencyMapping.ContainsKey(c1))
            {
                throw new CurrencyDoesNotExistsException("Currency Does Not exist", c1);
            }
            int i = 0;
            for (i = 0; i < CurrencyMapping[c1].Count; ++i)
            {
                if (CurrencyMapping[c1][i].Key == c2)
                {
                    break;
                }
            }

            if (i < CurrencyMapping[c1].Count)
            {
                CurrencyMapping[c1][i] = new KeyValuePair<Currency, float>(c2, v);
            }
            else
            {
                CurrencyMapping[c1].Add(new KeyValuePair<Currency, float>(c2, v));
            }

        }

        static public float Calculate(Currency c1, Currency c2, float num)
        {
            if (!CurrencyMapping.ContainsKey(c1))
            {
                throw new CurrencyDoesNotExistsException("Currency Does Not exist", c1);
            }
            int i = 0;
            for (i = 0; i < CurrencyMapping[c1].Count; ++i)
            {
                if (CurrencyMapping[c1][i].Key == c2)
                {
                    break;
                }

            }
            if (i < CurrencyMapping[c1].Count)
            {
                return CurrencyMapping[c1][i].Value * num;
            }
            else
            {
                throw new CurrencyDoesNotExistsException("Currency Does Not exist", c2);
            }
        }
    }
}