﻿using System;
using System.Collections.Generic;

namespace FindTheNumber
{
    class Program
    {
        static int CountOne(int[] arr)
        {
            #region first way
            //bool x = false;
            //for (int i = 0; i < arr.Length; i++)
            //{

            //    for (int j = 0; j < arr.Length; j++)
            //    {
            //        if (i != j && arr[i] == arr[j])
            //        {
            //            x = true;
            //        }
            //    }
            //    if (x == false)
            //        return arr[i];
            //    else
            //        x = false;
            //}
            //return -1;
            #endregion


            #region second way
            //Array.Sort(arr);
            //int res = 0;
            //for (int i = 0; i < arr.Length-1; i+=2)
            //{
            //    res += arr[i];
            //    res -= arr[i + 1];
            //}
            //return res;
            #endregion

            #region third way   
            /* int max = 0;
             for (int i = 0; i < arr.Length; i++)
             {
                 if (arr[i] > max)
                 {
                     max = arr[i];
                 }
             }
             int[] arr1 = new int[max];
             for (int i = 0; i < arr1.Length; ++i)
             {
                 ++arr1[arr[i]];
             }
             for (int i = 0; i < arr1.Length; i++)
             {
                 if (arr[i] == 1)
                     return arr[i];
             }

             return - 1;*/
            #endregion

            #region forth way
            //var dict = new Dictionary<int, int>();
            //bool x = false;
            //int index = -1;
            //for (int i = 0; i < arr.Length; ++i)
            //{

            //    if (dict.Count != 0)
            //    {

            //        foreach (var item in dict.Keys)
            //        {
            //            if (item == arr[i])
            //            {
            //                index = item;
            //                x = true;
            //            }
            //        }
            //    }
            //    if (!x)
            //    {
            //        dict.Add(arr[i], 1);
            //    }
            //    else
            //    {
            //        ++dict[index];
            //        x = false;

            //    }
            //}
            //foreach (var item in dict.Keys)
            //{
            //    if (dict[item] == 1)
            //        return item;
            //}
            //return -1;
            #endregion

            #region best way
            int res = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                res ^= arr[i];
            }
            return res;
            #endregion
        }
        static void Main(string[] args)
        {
            int[] arr = { 1, 2, 3, 4, 3, 2, 1 };
            Console.WriteLine(CountOne(arr));
            Console.ReadLine();
        }
    }
}
