﻿using System;

namespace SubstringCount
{
    class Program
    {
        static void Main(string[] args)
        {
           // Console.WriteLine(SubstrCount("abbgb"));
            SubstrIndex("abbaabcdabb","abb");
            Console.ReadKey();
        }
        static double SubstrCount(string a)
        {
            return a.Length%2==1 ? ((1+a.Length)/2) * a.Length:(((1+a.Length)/ 2)+0.5)  * (a.Length);
            //if a.length is not odd  number 1+a.length/2 will not contain the 0.5 of the result f.g(((1+4)/2)*4=8) but (((1+4)/2) +0.5 )*4=10
        }
        static void SubstrIndex(string a,string sub)
        {
            for (int i = 0; i <= a.Length-sub.Length; ++i)
            {
                if (a.Substring(i, sub.Length)==sub)
                {
                    Console.WriteLine(i);
                }
            }
        }
    }
}
