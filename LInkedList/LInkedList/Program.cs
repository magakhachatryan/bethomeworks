﻿using System;

namespace LInkedList
{
    class Program
    {
        static void Main(string[] args)
        {
            MyList list = new MyList();
            list.PushBack(4);
            list.PushBack(5);
            list.PushAt(1,6);
            list.PushAt(1, 6);
            list.PushAt(4,7);          
            list.PushAt(4, 6);
            list.PushAt(2, 4);
            list.PushAt(2, 5);
            list.PushAt(1, 6);
            list.PushAt(1, 6);
            list.PushAt(10, 7);
            Console.WriteLine(list.length);

            Console.WriteLine();
            list.Print();
            Console.WriteLine();
     
            Console.WriteLine(list.length);
            Console.WriteLine();
            list.DeleteNode();
            list.Print();

        }
    }
}
