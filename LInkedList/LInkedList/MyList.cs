﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LInkedList
{
    public class Node
    {
        public int data;
        public Node next;
        
        
        public Node()
        {

        }
        public Node(int data)
        {
            this.next = null;
            this.data = data;
        }
    }

    public  class MyList
    {
        Node first;
        Node current;
        public                                    int length;
        public  MyList()
        {
           
        }


        Node find(Node node)
        {
            Node temp = first;
            while (temp != null)
            {
                if (temp == node)
                    return temp;
                temp = temp.next;
            }
            return null;
        }

        #region PUSH FUNCTIONS
        public void PushBack(int data)
        {
            if(first==null)
            {
                first = new Node(data);
                current = first;
            }
            else
            {
                Node temp = new Node(data);
                current.next = temp;
                current = temp;
            }
            ++length;
        }

        private void PushAfter(Node node,int data)
        {
            Node temp = find(node);
            if (temp != null)
            {
                Node newitem = new Node(data);
                newitem.next = node.next;
                node.next = newitem;
                if (temp == current)
                {
                    current = newitem;
                }
            }
            ++length;
            
        }
        public void PushAt(int x,int data)
        {
            Node temp = first;
            for(int i=1;i<x-1;++i)
            {
                temp = temp.next;
            }
            if (temp!=null)
            {
                if (x == 1)
                {
                    Node curr = first;
                    first = new Node(data);
                    first.next = curr;
                    ++length;
                }
                else
                { PushAfter(temp, data); }
                
            }

        }
        
        public void PushFromBeggining(int data)
        {
            
            PushAt(0,data);
        }
        #endregion

        
        #region REMOVE FUNCTION
        public int Remove(int a)
        {
            Node temp = first;
            int count =0;
            if(length==1 && first.data==a)
            {
                first = null;
                current = null;
                ++count;
                --length;
            }
            while(temp.next!=null)
            {
                    if (first.data == a)
                    {
                        temp = temp.next;
                        first = temp;
                         ++count;
                         --length;
                    }
                    else
                    {
                    if (temp.next.data == a)
                    {
                        RemoveAfter(temp);
                        ++count;

                    }
                    else
                        temp = temp.next;
                    }
                }
                
            return count;
        }

        public void RemoveAfter(Node item)
        {
            Node temp = first;
            while (temp != item)
            {
                temp = temp.next;
            }
            if (temp.next == null)
            {
                return;
            }
            if (temp.next == current)
            {
                current = temp;
                temp.next = null;
            }
            else
            {
                temp.next = temp.next.next;
               // temp.next.next = null;
            }
            --length; ;
        }
        #endregion
        public void Print()
        {
            Node temp = first;
            while(temp!=null)
            {
                Console.WriteLine(temp.data);
                temp = temp.next;
            }
        }


        public void DeleteNode()
        {
            Node temp = first;
            var set = new HashSet<int>();
         
            set.Add(temp.data);
            
                while (temp.next != null)

                {
                    if (set.Contains(temp.next.data)) 
                    {

                        RemoveAfter(temp);
                    }
                    else
                    {
                        set.Add(temp.next.data);
                        temp = temp.next;

                    }
                }
                
               
               
            



        }
    }
}
