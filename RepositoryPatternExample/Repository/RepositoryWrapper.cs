﻿using System;
using System.Collections.Generic;
using System.Text;
using Entities;
using Contracts;

namespace Repository
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private RepositoryContext repositoryContext;
        private IOwnerRepository owner;
        private IAccountRepository account;

        public IOwnerRepository Owner
        {
            get
            {
                if (owner == null)
                { 
                    owner = new OwnerRepository(repositoryContext); 
                }
                return owner;
            }
        }


        public IAccountRepository Account
        {
            get
            {
                if (account == null)
                {
                    account = new AccountRepository(repositoryContext);
                }
                return account;
            }
        }

        public RepositoryWrapper(RepositoryContext repoContext)
        {
            repositoryContext = repoContext;
        }

        public void Save()
        {
            repositoryContext.SaveChanges();
        }
    }
}
