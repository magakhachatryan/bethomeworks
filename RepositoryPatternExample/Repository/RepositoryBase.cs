﻿using Contracts;
using Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;


namespace Repository
{
    public abstract class RepositoryBase<T> : IRepository<T> where T : class
    {
        protected RepositoryContext repositoryContext { get; set; }

        public RepositoryBase(RepositoryContext repositoryContext)
        {
            this.repositoryContext = repositoryContext;
        }

        public IQueryable<T> FindAll()
        {
            return this.repositoryContext.Set<T>().AsNoTracking();
        }

       
        public void Create(T entity)
        {
            this.repositoryContext.Set<T>().Add(entity);
        }

        public void Update(T entity)
        {
            this.repositoryContext.Set<T>().Update(entity);
        }

        public void Delete(T entity)
        {
            this.repositoryContext.Set<T>().Remove(entity);
        }
    }
}

