﻿using Entities.Models;

namespace Contracts
{
    public interface IAccountRepository:IRepository<Account>
    {
    }
}
