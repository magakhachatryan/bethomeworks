﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Models
{
    [Table("owner")]
    public class Owner
    {
        public Guid OwnerId { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [StringLength(70, ErrorMessage = "Name can contain mostly 70 characters")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Date Of Birth is required")]
        public DateTime DateOfBirth { get; set; }

        [Required(ErrorMessage = "Adress is required")]
        [StringLength(100, ErrorMessage = "Adress can mostly contain 100 characters")]
        public string Adress { get; set; }

        public ICollection<Account> Accounts { get; set; }
    }
}
