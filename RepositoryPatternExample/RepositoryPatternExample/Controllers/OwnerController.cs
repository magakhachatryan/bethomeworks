﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using Entities.Models;

namespace RepositoryPatternExample.Controllers
{
    [Route("api/owner")]
    [ApiController]
    public class OwnerController : ControllerBase
    {
        private ILogger<OwnerController> logger;
        private IRepositoryWrapper wrapper;

        public OwnerController(ILogger<OwnerController> logger, IRepositoryWrapper wrapper)
        {
            this.logger = logger;
            this.wrapper = wrapper;
        }

        [HttpGet]
        public ActionResult GetAllOwners()
        {
            try
            {
                var owners = wrapper.Owner.GetAllOwners();
                logger.LogInformation("Returned all owners from db");
                return Ok(owners);
            }
            catch (Exception ex)
            {
                logger.LogInformation($"Something went wrong : {ex.Message}");
                return StatusCode(500, "Internal Server Error");
            }
        }


        
    }
}