﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DITypesManual
{
    public class Buyer
    {
        Service service;
        //using ctor
       public  Buyer(Service item)
        {
            this.service = item;
        }
        
        /*using property
        public Service Service
        {
            set { this.service = value; }
        }*/

        /*using method
        public void StartMethod(Service item)
        {
            this.service=item
        }*/
        public void Buy()
        {
            this.service.Buy();
        }
    }
}
