﻿using System;
using System.Reflection;
using Ninject;

namespace DITypesManual
{
    class Program
    {
        static void Main(string[] args)
        {
            //Manual
            MiniMarket mini = new MiniMarket();
            Buyer user = new Buyer(mini);
            user.Buy();
            

            //using NInject
            //ste chem haskanum imasty sra?? 
            //ete mi hat classi enq karoxanum bind anenq 
            //haskanum em vor urish class, vor urish interface implement ani da karanq bind anenq
            //aysinqn ete irar mej shat dep.inject-ner kan tenc depqerum lava
            //bayc nuyn interface imolement anox classneric mekina bind anum...che???

            var kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());
            var service = kernel.Get<Service>();

            var buyer = new Buyer(service);
            buyer.Buy();
            Console.ReadLine();
        }
    }
}
