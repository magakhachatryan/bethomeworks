﻿using Ninject.Modules;

namespace DITypesManual
{
    public class Bindings : NinjectModule
    {
        public override void Load()
        {
            Bind<Service>().To<Supermarket>();
        }
    }
}
