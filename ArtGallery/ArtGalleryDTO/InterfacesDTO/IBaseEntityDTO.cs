﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArtGalleryDTO.InterfacesDTO
{
    public interface IBaseEntityDTO
    {
        public int Id { get; set; }
    }
}
