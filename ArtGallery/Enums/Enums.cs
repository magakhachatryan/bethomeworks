﻿namespace Enums
{
    public enum Types:int
    {
        Painting = 0,
        Sculpture = 1,
        Fabric = 2,
        Other = 3,
    }
}
