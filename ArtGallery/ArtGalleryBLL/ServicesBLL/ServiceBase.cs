﻿using System;
using System.Collections.Generic;
using System.Text;
using ArtGalleryBLL.InterfacesBLL;
using ArtGalleryDAL.Interfaces;
using ArtGalleryDTO.InterfacesDTO;

namespace ArtGalleryBLL.ServicesBLL
{
    class ServiceBase<TEntity,TDto> : IBaseService<TEntity, TDto>
       where TEntity : class, IBaseEntity
       where TDto : class, IBaseEntityDTO
    {
        public TDto Add(TDto model)
        {
           
        }

        public List<TDto> Get(System.Linq.Expressions.Expression<Func<TEntity, bool>> expression)
        {
           
        }

        public List<TDto> GetAll()
        {
           
        }

        public TDto GetById(int id)
        {
            
        }

        public TDto Remove(int id)
        {
           
        }

        public TDto Update(TDto model)
        {
            
        }
    }
}
