﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace ArtGalleryBLL.InterfacesBLL
{
    public interface IBaseService<TEntity, TDto>
    {
        TDto Add(TDto model);
        List<TDto> GetAll();
        TDto GetById(int id);
        List<TDto> Get(Expression<Func<TEntity, bool>> expression);
        TDto Remove(int id);
        TDto Update(TDto model);
    }
}
