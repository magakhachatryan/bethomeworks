﻿using System;
using System.Collections.Generic;
using ArtGalleryDTO.EntitiesDTO;
using ArtGalleryDAL.Entities;


namespace ArtGalleryBLL.InterfacesBLL
{
   public interface IUserService : IBaseService<User,UserDTO>
    {

    }
}
