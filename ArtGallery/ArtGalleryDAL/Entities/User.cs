﻿using System.Collections.Generic;

namespace ArtGalleryDAL.Entities
{
    public class User : IBaseEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Account { get; set; }
        protected string LogIn { get; set; }
        protected string Password { get; set; }

        public List<ArtObject> Objects { get; set; }
        
    }
}

