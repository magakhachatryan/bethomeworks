﻿using System;
using Microsoft.EntityFrameworkCore;
using ArtGalleryDAL.Entities;
namespace ArtGalleryDAL
{
    public class ArtGalleryContext : DbContext
    {

        public ArtGalleryContext(DbContextOptions<ArtGalleryContext> options) : base(options) { }

        public DbSet<User> Users { get; set; }
        public DbSet<ArtObject> Objects { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ArtObject>().HasOne(p => p.user)
                                            .WithMany(p => p.Objects)
                                            .HasForeignKey(p => p.UserId);
        }
    }
}
