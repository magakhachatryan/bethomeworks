﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SameStringCanBeCreated
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(SameStr("anbn", "nnab"));
            Console.ReadKey();
        }

        static public bool SameStr(string one,string two)
        {
            if(one.Length!=two.Length)
            {
                return false;
            }
            int k = 0;
            int[] arr = new int[2*one.Length];
            for (int i = 0; i < one.Length; i++)
            {
                arr[k] = Convert.ToInt32(one[i]);
                arr[++k] = Convert.ToInt32(two[i]);
                ++k;
            }
            int res = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                res ^= arr[i];
            }

            return res == 0 ? true : false;

        }
    }
}
